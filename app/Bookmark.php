<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmark extends Model
{

  protected $dates = ['deleted_at'];

  public function users()
  {
      return $this->belongsTo('App\User');
  }
}
