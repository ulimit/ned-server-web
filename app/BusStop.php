<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusStop extends Model
{
    //
    protected $fillable =['code','description','road_name','latitude','longitude'];
}
