<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Bookmark;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BookmarkController extends Controller
{
    public function update(Request $request){
      $object = Bookmark::where('id', $request->id)->first();
      $object->title = $request->title;
      $object->save();
      return 200;
    }

    public function destroy($id){
      $object = Bookmark::destroy($id);
      return 200;
    }

    public function listings(Request $request){

      if(!isset($request->owner) || empty($request->owner))
        echo json_encode(array('status' => 'error', 'message' => 'Owner not found or is empty'));

      $user = User::whereRaw('email = ? or id = ?',[$request->owner,$request->owner])->first();
      if($user == null)
       echo json_encode(array('status' => 'error', 'message' => 'Owner not found or is empty'));
      else{
        $objectList = Bookmark::where('user_id',$user->id)
         ->orderBy('created_at', 'asc')
         ->take(100)
         ->get();

        return $objectList->toJson();
      }
    }

    public function post(Request $request){

      $responseOrigin = $this->geoCode($request->origin);
      $responseDestination = $this->geoCode($request->destination);

      if(isset($request->id) && !empty($request->id))
        $object = Bookmark::where('id',$request->id)->first();
      else
        $object = new Bookmark;

      $object->user_id = $request->owner;
      $object->title = $request->title;

      $originLatLng = $responseOrigin['results'][0]['geometry']['location'];
      $destinationLatLng = $responseDestination['results'][0]['geometry']['location'];

      $object->origin = $responseOrigin['results'][0]['place_id'];
      $object->formatted_origin = $responseOrigin['results'][0]['formatted_address'];
      $object->origin_latlng = $originLatLng['lat'] . ',' . $originLatLng['lng'];

      $object->destination = $responseDestination['results'][0]['place_id'];
      $object->formatted_destination = $responseDestination['results'][0]['formatted_address'];
      $object->destination_latlng = $destinationLatLng['lat'] . ',' . $destinationLatLng['lng'];

      $object->transit_options = $request['transitOptions'];
      $object->route_index = $request['routeIndex'];
      
      $object->save();

      return 200;

    }

    function getGeocodeType($var){

      if (strpos((string)$var, 'placeId') !== false) {
          return 'placeid';
      }
      else if(strpos((string)$var, 'lat') !== false && strpos((string)$var, 'lng') !== false){
          return 'latlng';
      }
      else return 'error';

    }

    function geoCode($str){
      $type = $this->getGeocodeType($str);

      $key = 'AIzaSyDtP07Qw64UrLfVzeHyHzEc8DytTt-kBPQ';
      $queryStr = '';

      $queryParam = json_decode($str);

      if($type == 'latlng'){
        $queryStr = '?latlng=' . $queryParam->lat . ',' . $queryParam->lng;
      }else if($type == 'placeid'){
        $queryStr = '?place_id=' . $queryParam->placeId;
      }

      $queryStr = $queryStr . '&key=' . $key;
      $queryStr = "https://maps.googleapis.com/maps/api/geocode/json" . $queryStr;

      return json_decode(file_get_contents($queryStr), true);
    }
}
