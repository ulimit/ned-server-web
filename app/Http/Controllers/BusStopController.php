<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BusStop;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BusStopController extends Controller
{
    public function storeBusStop(Request $request){
      if(isset($request->id) && !empty($request->id))
        $object = BusStop::where('id',$request->id)->first();
      else
        $object = new BusStop;

//      $object->bus_stop_id = $request->bus_stop_id;
//      $object->address = $request->address;
//      $object->coordinates = $request->coordinates;
//      $object->status = $request->status;

      $object->save();
      return 200;
    }

    public function busLookup(){

      $ch = curl_init();

      //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL, 'http://datamall2.mytransport.sg/ltaodataservice/BusArrival?BusStopID=01012');

      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'AccountKey: YTTfxmD03bVay8WPMFJolQ==',
        'UniqueUserID: 0736283f-dea8-41de-9f73-4135084a22f6',
        'accept: application/json'
      ));

      return curl_exec($ch);
    }
    
    public function getBusStopList(){
      $objectList = BusStop::orderBy('created_at', 'asc')
       ->take(100)
       ->get();

      return $objectList->toJson();
    }

    public function destroy($id){
      $object = BusStop::destroy($id);
      return 200;
    }
}
