<?php

namespace App\Http\Controllers;

use App\Auth;
use App\Setting;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      return view('home',['title' => 'Home']);
    }

    public function bookmarks(Request $request)
    {
      return view('bookmark',['title' => 'My Bookmarks']);
    }

    public function logs(Request $request)
    {
      return view('log',['title' => 'Travel Logs']);
    }

    public function bus(Request $request)
    {
      return view('bus',['title' => 'Bus Stops']);
    }

    public function settings(Request $request)
    {
      $user_id = $request->user()->id;
      $setting = Setting::where('user_id', $request->user()->id)->first();

      return view('setting',['title' => 'Settings','object' => $setting]);
    }
    public function returns(){
      return 200;
    }
}
