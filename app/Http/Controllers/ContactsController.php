<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contacts;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
  public function getList($user_id){
    $objectList = Contacts::whereRaw('user_id = ?', array($user_id))
     ->take(10)
     ->get();

    return $objectList->toJson();
  }

  public function getObject($id){
    $object = Contacts::where('id', $id)->first();
    return $object->toJson();
  }

  public function destroyObject($id){
    $object = Contacts::destroy($id);
    return 200;
  }

  public function saveObject(Request $request){

    if(isset($request->id) && !empty($request->id))
      $object = Contacts::where('id',$request->id)->first();
    else
      $object = new Contacts;

    $object->user_id = $request->user_id;
    $object->name = $request->name;
    $object->contact = $request->contact;
    $object->default = false; //$request->default;

    $object->save();

    return 200;
  }

  public function updateObject(Request $request){
    $object = Contacts::where('id',$_POST['id'])->first();

    $object->name = $_POST['name'];
    $object->contact = $_POST['contact'];

    $object->save();

    return 200;
  }

  public function setDefault(){
    Contacts::where('id','<>','')->update(['default' => false]);
    Contacts::where('id','=',$_POST['id'])->update(['default' => true]);
    return 200;
  }
}
