<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class JourneyController extends Controller
{
  public function destroy($id){
    $object = Journey::destroy($id);
    return 200;
  }

  public function listings(Request $request){

    $user = User::whereRaw('email = ? or id = ?',[$request->owner,$request->owner])->first();

    $objectList = Journey::where('user_id',$user->id)
     ->orderBy('created_at', 'asc')
     ->take(100)
     ->get();

    return $objectList->toJson();
  }

  public function post(Request $request){

    if(isset($request->id) && !empty($request->id))
      $object = Journey::where('id',$request->id)->first();
    else
      $object = new Journey;

    $object->user_id = $request->owner;
    $object->journeyId = $request->journeyId;
    $object->start = $request->start;
    $object->end = $request->end;

    $object->save();

    return 200;

  }

}
