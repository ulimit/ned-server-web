<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogController extends Controller
{

  public function destroy($id){
    $object = Log::destroy($id);
    return 200;
  }

  public function listings(Request $request){

    $user = User::whereRaw('email = ? or id = ?',[$request->owner,$request->owner])->first();

    $objectList = Log::where('user_id',$user->id)
     ->orderBy('created_at', 'asc')
     ->take(100)
     ->get();

    return $objectList->toJson();
  }

  public function post(Request $request){

    if(isset($request->id) && !empty($request->id))
      $object = Log::where('id',$request->id)->first();
    else
      $object = new Log;

    $object->user_id = $request->owner;
    $object->journey = $request->journey;

    $object->save();

    return 200;

  }

  function getGeocodeType($var){

    if (strpos((string)$var, 'placeId') !== false) {
        return 'placeid';
    }
    else if(strpos((string)$var, 'lat') !== false && strpos((string)$var, 'lng') !== false){
        return 'latlng';
    }
    else return 'error';

  }

  function geoCode($str){
    $type = $this->getGeocodeType($str);

    $key = 'AIzaSyDtP07Qw64UrLfVzeHyHzEc8DytTt-kBPQ';
    $queryStr = '';

    $queryParam = json_decode($str);

    if($type == 'latlng'){
      $queryStr = '?latlng=' . $queryParam->lat . ',' . $queryParam->lng;
    }else if($type == 'placeid'){
      $queryStr = '?place_id=' . $queryParam->placeId;
    }

    $queryStr = $queryStr . '&key=' . $key;
    $queryStr = "https://maps.googleapis.com/maps/api/geocode/json" . $queryStr;

    return json_decode(file_get_contents($queryStr), true);
  }
}
