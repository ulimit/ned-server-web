<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;
use App\User;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

  public function login(Request $request){

    $object = User::where('email', $request->email)->first();

    $setting = null;


    if($object != null){
      $setting = Setting::where('user_id', $object->id)->first();
    }

    echo json_encode(array(
      'user' => $object,
      'setting' => $setting
    ));
  }

  public function register(Request $request){

    $object = new User;

    $object->name = $request->name;
    $object->email = $request->email;
    $object->password = bcrypt($request->password);

    $object->save();

    $generatedApi = md5(microtime().rand());

    Setting::create([
      'user_id' => $object->id,
      'api_key' => $generatedApi
    ]);

    echo json_encode(array('200'));
  }
}
