<?php

namespace App\Http\Controllers;

use App\BusRoute;
use App\BusStop;
use App\Subway;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class LTAController extends Controller
{
    var $BASE_URL = "http://datamall2.mytransport.sg/ltaodataservice/";
    var $BUS_ARRIVAL= "BusArrival";
    var $BUS_STOP= "BusStops";
    var $BUS_ROUTE= "BusRoutes";
    var $HEADER = ['AccountKey:YTTfxmD03bVay8WPMFJolQ==',
                    'UniqueUserID:0736283f-dea8-41de-9f73-4135084a22f6',
                    'accept: application/json'];


    public function getArrival(Request $request)
    {
        $stopID = $request->get('stop_id');
        $busNo = $request->get('bus');
        return $this->getArrivalFromLTA($stopID,$busNo);
    }

    public function getArrivalByStopCoordinate(Request $request){
        $latitude = $request->get('lat');
        $longitude = $request->get('lng');
        $serviceNo = $request->get('bus');
	
	$latitude = round($latitude,7);
        $longitude = round($longitude,7);

        $busStop = BusStop::where('latitude',$latitude)->where('longitude',$longitude)->first();
        if($busStop){
            return $this->getArrivalFromLTA($busStop->code,$serviceNo);
        }
        else{
	   return [];
        }
    }

   public function getInBetweenBusStop(Request $request){
        $dep_lat = $request->get('dep_lat');
        $dep_lng = $request->get('dep_lng');
        $arr_lat = $request->get('arr_lat');
        $arr_lng = $request->get('arr_lng');
        $busNo = $request->get('bus');

	$dep_lat = round($dep_lat,7);
        $dep_lng = round($dep_lng,7);
        $arr_lat = round($arr_lat,7);
        $arr_lng = round($arr_lng,7);

	$busStop1 = BusStop::where('latitude',$dep_lat)->where('longitude',$dep_lng)->first();
        $busStop2= BusStop::where('latitude',$arr_lat)->where('longitude',$arr_lng)->first();

	$busRoute1 = BusRoute::where('bus_no',$busNo)
            ->where('bus_stop_code',$busStop1->code)->first();

        $busRoute2 = BusRoute::where('bus_no',$busNo)
            ->where('bus_stop_code',$busStop2->code)
            ->where('sequence','>',$busRoute1->sequence)
	    ->first();

        $busRoute= BusRoute::with('busStop')->where('bus_no',$busNo)
            ->where('sequence','>',$busRoute1->sequence)
            ->where('sequence','<',$busRoute2->sequence)
            ->get();

        /*$sql ="SELECT s3.code, s3.description,s3.road_name, s3.latitude, s3.longitude
        FROM bus_routes r1,
        bus_routes r2,
        bus_stops s3,bus_routes r3
        where s3.code = r3.bus_stop_code
        AND r1.sequence < r3.sequence
        AND r3.sequence < r2.sequence
        AND r1.bus_no = r3.bus_no
        AND r2.bus_no = r3.bus_no";

        $sql .=" AND r1.bus_stop_code = ". $busStop1->code.
        " AND r2.bus_stop_code = ".$busStop2->code.
        " AND r3.bus_no = ".$busNo;
        $bus_stops = DB::select(DB::raw($sql));
         */
	return $busRoute;
    }

    public function getInBetweenSubway(Request $request){
        $dep_lat = $request->get('dep_lat');
        $dep_lng = $request->get('dep_lng');
        $arr_lat = $request->get('arr_lat');
        $arr_lng = $request->get('arr_lng');
        $line = $request->get('line');

        $dep_lat = round($dep_lat,7);
        $dep_lng = round($dep_lng,7);
        $arr_lat = round($arr_lat,7);
        $arr_lng = round($arr_lng,7);

        $subway1 = Subway::where('line_code',$line)
            ->where('latitude',$dep_lat)
            ->where('longitude',$dep_lng)->first();

        $subway2 = Subway::where('line_code',$line)
            ->where('latitude',$arr_lat)
            ->where('longitude',$arr_lng)->first();

        if($subway1->sequence < $subway2->sequence){
            $subways = Subway::where('line_code',$line)
                ->where('sequence','>',$subway1->sequence)
                ->where('sequence','<',$subway2->sequence)
		->orderby('sequence')->get();
        }else{
            $subways = Subway::where('line_code',$line)
                ->where('sequence','<',$subway1->sequence)
                ->where('sequence','>',$subway2->sequence)
		->orderby('sequence','desc')->get();
        }
        return $subways;
    }

    public function getBusStop(){

    }

    public function getBusRoute(){

    }

    public function getArrivalFromLTA($stopID,$busNo){
        $url = $this->BASE_URL.$this->BUS_ARRIVAL
            .'?BusStopID='.$stopID
            .'&SST=true';

        if(isset($busNo)){
            $url.='&ServiceNo='.$busNo;
        }
        return $this->executeCURL($url,$this->HEADER);
    }

    public function getBusStops(){
        $url = $this->BASE_URL.$this->BUS_STOP;
        $hasvalue = true;
        $i = 0;

        while($hasvalue) {
            $tmp_url = $url .'?$skip='.($i*50);
            $data = $this->executeCURL($tmp_url, $this->HEADER);
            $data = json_decode($data);
            if(!$data->value){
                $hasvalue = false;
            }else {
                foreach ($data->value as $stop) {
                    $busStop = BusStop::where('code',$stop->BusStopCode)->first();
                    if(!$busStop) {
                        echo 'I am new busstop'.$stop->BusStopCode;
                        $busStop = new BusStop();
                        $busStop->code = $stop->BusStopCode;
                    }
                    $busStop->description = $stop->Description;
                    $busStop->road_name = $stop->RoadName;
                    $busStop->latitude = round($stop->Latitude,7);
                    $busStop->longitude = round($stop->Longitude,7);
                    $busStop->save();
                }
            }
            $i++;
        }
       echo $i;
    }

    public function getBusRoutes(){
        $url = $this->BASE_URL.$this->BUS_ROUTE;
        $hasvalue = true;
        $i = 0;

        while($hasvalue) {
            $tmp_url = $url . '?$skip=' . ($i * 50);
            $data = $this->executeCURL($tmp_url, $this->HEADER);
            $data = json_decode($data);
            if(!$data->value){
                $hasvalue = false;
            }else {
                foreach ($data->value as $route) {
                    $busRoute = BusRoute::where('bus_no',$route->ServiceNo)
			->where('bus_stop_code',$route->BusStopCode)
			->where('sequence',$route->StopSequence)->first();
                    if(!$busRoute){
                        //echo 'I am here';
                        $busRoute = new BusRoute();
                        $busRoute->bus_no = $route->ServiceNo;
                        $busRoute->bus_stop_code = $route->BusStopCode;
                    }
                    $busRoute->operator = $route->Operator;
                    $busRoute->direction = $route->Direction;
                    $busRoute->sequence = $route->StopSequence;
                    $busRoute->distance = ($route->Distance?$route->Distance:0);
                    $busRoute->save();
                }
            }
            $i++;
        }
        echo $i;
    }

    public function executeCURL($url, $header){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HTTPHEADER,$header) ;
        $json = curl_exec($ch);
        return $json;
    }
}
