<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Setting;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    public function store(Request $request){
      if(isset($request->setting_id) && !empty($request->setting_id))
        $object = Setting::where('id',$request->setting_id)->first();
      else
        $object = new Setting;

      $object->contact_one_name = $request->contact;
      $object->contact_one_number = $request->contact_number;

      $object->contact_two_name = $request->alternate;
      $object->contact_two_number = $request->alternate_number;

      $object->alert_radius = $request->alert_radius;

      $object->save();

      return redirect('/home#settings');
    }

    public function getContacts(Request $request){

      if(!isset($request->owner) || empty($request->owner))
        echo json_encode(array('status' => 'error', 'message' => 'Owner not found or is empty'));

      $user = User::whereRaw('email = ? or id = ?',[$request->owner,$request->owner])->first();

      if($user == null)
       echo json_encode(array('status' => 'error', 'message' => 'Owner not found or is empty'));
      else{
        $setting = Setting::where('user_id', $user->id)->first();

        $aContacts['contact'] = $setting['contact_one_name'];
        $aContacts['contact_number'] = $setting['contact_one_number'];
        $aContacts['alternate'] = $setting['contact_two_name'];
        $aContacts['alternate_number'] = $setting['contact_two_number'];

        echo json_encode($aContacts);
      }
    }


}
