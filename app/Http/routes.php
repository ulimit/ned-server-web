<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home',['title' => 'Home']);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

  Route::post('/v1/api/authenticate', ['middleware' => 'auth.basic', function() {
      return "200";
  }]);

  Route::post('/v1/api/register','UserController@register');

  Route::group(['middleware' => 'auth.basic'], function(){

    Route::post('/v1/api/authenticate', 'UserController@login');
    Route::get('/v1/api/contacts', 'SettingController@getContacts');
    Route::post('/v1/api/bookmark/post','BookmarkController@post');
    Route::get('/v1/api/bookmark/list','BookmarkController@listings');
    Route::post('/v1/api/bookmark/destroy/{id}','BookmarkController@destroy');
    Route::post('/v1/api/bookmark/update','BookmarkController@update');
    Route::post('/v1/api/log/post','LogController@post');
    Route::get('/v1/api/log/list','LogController@listings');

    Route::post('/v1/api/journey/post','JourneyController@post');
    Route::get('/v1/api/journey/list','JourneyController@listings');
    Route::post('/v1/api/journey/destroy/{id}','JourneyController@destroy');

    Route::post('/v1/api/bus/post','BusStopController@storeBusStop');
    Route::post('/v1/api/bus/destroy/{id}','BusStopController@destroy');
   //Add by simon 20160227
    //Route::post('/v1/api/sendsms','SMSController@send');
});

Route::post('/v1/api/sendsms','SMSController@send');

Route::get('/bus/list','BusStopController@busLookup');
Route::get('/v1/api/bus/list','BusStopController@getBusStopList');

Route::get('/bus/arrival','LTAController@getArrivalByStopCoordinate');
Route::get('/bus/stops','LTAController@getBusStops');
Route::get('/bus/routes','LTAController@getBusRoutes');
Route::get('/bus/inBetween','LTAController@getInBetweenBusStop');
Route::get('/subway/inBetween','LTAController@getInBetweenSubway');

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::get('/bus', 'HomeController@bus');
    Route::get('/bookmarks', 'HomeController@bookmarks');
    Route::get('/logs', 'HomeController@logs');
    Route::get('/settings', 'HomeController@settings');

    Route::post('/setting/store', 'SettingController@store');

    Route::post('/bookmark/post','BookmarkController@post');
    Route::get('/bookmark/list','BookmarkController@listings');
    Route::post('/bookmark/destroy/{id}','BookmarkController@destroy');
    Route::post('/bookmark/update','BookmarkController@update');

    Route::post('/log/post','LogController@post');
    Route::get('/log/list','LogController@listings');
    Route::post('/log/destroy/{id}','LogController@destroy');

    Route::post('/journey/post','JourneyController@post');
    Route::get('/journey/list','JourneyController@listings');
    Route::post('/journey/destroy/{id}','JourneyController@destroy');

    Route::post('/bus/post','BusStopController@storeBusStop');
    Route::post('/bus/destroy/{id}','BusStopController@destroy');

});
