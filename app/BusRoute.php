<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusRoute extends Model
{
    //
    public function busStop(){
        return $this->belongsTo(BusStop::class,'bus_stop_code','code');
    }
}
