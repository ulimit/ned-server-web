<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setting()
    {
        return $this->hasOne('App\Setting');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark');
    }

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    public function journeys()
    {
        return $this->hasMany('App\Journey');
    }
}
