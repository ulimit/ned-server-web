<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journey extends Model
{
  protected $dates = ['deleted_at'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }
}
