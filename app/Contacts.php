<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
  protected $dates = ['deleted_at'];

  public function users()
  {
      return $this->belongsTo(User::class);
  }
}
