@extends('layouts.app')

@section('content')
<form method="POST" action="/setting/store">

  <input type="hidden" name="setting_id" id="setting_id" value="{{ $object->id }}"/>

  <div class="form-group">
    <label for="api_key">API Key</label>
    <input type="text" class="form-control" id="api_key" placeholder="API Key" disabled value="{{ $object->api_key }}">
  </div>

  <div class="form-group">
    <label for="contact">Contact Name</label>
    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact Name" value="{{ $object->contact_one_name }}">
  </div>
  <div class="form-group">
    <label for="contact_number">Contact Number</label>
    <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{ $object->contact_one_number }}">
  </div>

  <div class="form-group">
    <label for="alternate">Alternate Name</label>
    <input type="text" class="form-control" id="alternate" name="alternate" placeholder="Alternate Name" value="{{ $object->contact_two_name }}">
  </div>
  <div class="form-group">
    <label for="alternate_number">Alternate Number</label>
    <input type="text" class="form-control" id="alternate_number" name="alternate_number" placeholder="Alternate Number" value="{{ $object->contact_two_number }}">
  </div>

  <div class="form-group">
    <label for="alert_radius">Alert Radius</label>
    <input type="text" class="form-control" id="alert_radius" name="alert_radius" placeholder="Alert Radius" value="{{ $object->alert_radius }}">
  </div>

  <button type="submit" class="btn btn-default">Save Changes</button>
  <a href="/home" class="btn btn-warning">Back to Home page</a>
</form>
@endsection
