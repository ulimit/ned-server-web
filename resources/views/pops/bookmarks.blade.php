<h3 class="ui orange header">Bookmarks</h3>
<div class="ui divider"></div>

<table class="ui celled table">
  <thead>
    <tr><th colspan="3">
      Bookmarks
    </th>
  </tr></thead>
  <tbody class="bookmarks-list"></tbody>
</table>


<div class="ui small modal update-bookmark">
  <div class="header">Edit Bookmark</div>
  <div class="content">
    <div class="ui input">
      <input type="text" id="update-bookmark" />
    </div>

  </div>
  <div class="actions">
    <div class="ui approve green button bookmark-approve">Approve</div>
    <div class="ui cancel red button">Cancel</div>
  </div>
</div>


<script>
function getBookmarkList(){
  $.ajax({
    url: '/bookmarks/control?search=' + localStorage.getItem('selected_user'),
    async: false,
    success: function(data){
      var html = '';

      _.forEach(JSON.parse(data), function(value) {
        html += '<tr>';
          html += '<td style="font-size:12px;font-weight:bold">' + value.title + '</td>';
          html += '<td style="font-size:10px">From <strong>' + value.source + '</strong><br />';
          html += 'To <strong>' + value.destination + '</strong><br />';
          html += '<p style="color:gray">' + moment(value.created_at.date, 'YYYY-MM-DD HH:mm:ss.SSSS').fromNow() + '</p><a href="javascript:alert(\'View Functionlity Is Under Construction\')">View</a> | <a href="javascript:editBookmark(' + value.id + ',\'' + value.title + '\')">Edit</a> | <a href="javascript:deleteBookmark(' + value.id + ')">Delete</a></td>';
          // html += '<td style="font-size:12px"></td>';
        html += '</tr>';
      });

      if(html == '')
        html = 'No Bookmarks';

        $('.bookmarks-list').empty().append(html);
    }
  });
}

function editBookmark(id,title){
  $("#update-bookmark").val(title);
  $(".bookmark-approve").attr('onclick','updateBookmark(' + id + ')');
  $(".update-bookmark").modal('show');
}

function updateBookmark(id){
  ajaxPost('/bookmarks/updatetitle',{_token:'{{ csrf_token() }}',title: $("#update-bookmark").val(),id:id});
  getBookmarkList();
}

function deleteBookmark(id){
  if(confirm("Are you sure you want to delete?")){
    ajaxPost('/bookmarks/destroy/' + id, {_token:'{{ csrf_token() }}'});
    getBookmarkList();
  }
}
</script>
