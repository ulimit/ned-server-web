<h3 class="ui orange header">Contacts | <a href="javascript:contactForm()">Add New</a></h3>
<div class="ui divider"></div>

    <div class="ui divided items contact-list">
      No Contacts
    </div>

<div class="ui small modal contact-form-pop">
  <div class="header">Contact Form</div>
  <div class="content">

    <div class="ui form">
      <div class="field">
        <label>Contact Name</label>
        <input type="text" name="name" id="name" placeholder="Contact Name">
      </div>
      <div class="field">
        <label>Contact Number</label>
        <input type="text" name="contact" id="contact" placeholder="Contact Number">
      </div>
    </div>

  </div>
  <div class="actions">
    <button class="ui approve button contact-approve" onclick="saveContact()">Save</button>
    <button class="ui cancel button">Close</button>
  </div>
</div>

<script>
  $(document).ready(function(){
    getContactList();
  });

  function saveContact(){
    ajaxPost('/contacts/create', {
      _token: '{{ csrf_token() }}',
      user_id: localStorage.getItem('selected_user'),
      name: $("#name").val(),
      contact: $("#contact").val()
    });

    getContactList();
  }

  function getContactList(){
    var result = ajaxGet('/contacts/list/' + localStorage.getItem('selected_user'));
    var html = '';

    _.forEach(result, function(value) {

      html += '<div class="item">';
        html += ' <div class="content">';
           html += '<div class="meta">';
             html += '<span>' + value.name + ' | <a href="javascript:editContact(' + value.id + ',\'' + value.name + '\',\'' + value.contact + '\')">Edit</a></span> | <a href="javascript:deleteContact(' + value.id + ')">Delete</a></span>';
           html += '</div>';
           html += '<div class="extra">';
             html += value.contact;
             html += '<br />';
             if(value.default)
              html += '<a class="ui blue label">Default Contact</a>';
             else
              html += '<a href="javascript:setDefault(' + value.id + ')">Set As Default</a>';
           html += '</div>';
         html += '</div>';
       html += '</div>';

    });

    if(html == '')
      html = 'No Contacts';

      $(".contact-list").empty().append(html);

  }

  function editContact(id,name,contact){
    $("#name").val(name);
    $("#contact").val(contact);

    $(".contact-approve").attr('onclick','updateContact(' + id + ')');
    $(".contact-form-pop").modal('show');
  }

  function updateContact(id){
    ajaxPost('/contacts/update',{_token:'{{ csrf_token() }}',name: $("#name").val(),contact: $("#contact").val(),id:id});
    getContactList();
  }

  function setDefault(id){
    ajaxPost('/contacts/setdefault',{_token:'{{ csrf_token() }}',id:id});
    getContactList();
  }

  function deleteContact(id){
    if(confirm("Are you sure you want to delete?")){
      ajaxPost('/contacts/destroy/' + id, {_token:'{{ csrf_token() }}'});
      getContactList();
    }
  }

  function contactForm(){
    $("#name").val("");
    $("#contact").val("");

    $(".contact-approve").attr('onclick','saveContact()');
    $('.contact-form-pop').modal('show');
  }
</script>
