<div class="ui basic modal delete-pop">
  <i class="close icon"></i>
  <div class="header">
    Confirmation
  </div>
  <div class="image content">
    <div class="image">
      <i class="warning sign icon"></i>
    </div>
    <div class="description">
      <p>Are you sure you want to delete?</p>
    </div>
  </div>
  <div class="actions">
    <div class="two ui inverted buttons">
      <div class="ui red button">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui green inverted button">
        <i class="checkmark icon"></i>
        Yes
      </div>
    </div>
  </div>
</div>
