<h3 class="ui orange header">Logs</h3>
<div class="ui divider"></div>

<table class="ui celled table">
  <thead>
    <tr><th colspan="3">
      Logs
    </th>
  </tr></thead>
  <tbody class="logs-list"></tbody>
</table>


<script>
function getLogsList(){
  $.ajax({
    url: '/logs/control?search=' + localStorage.getItem('selected_user'),
    async: false,
    success: function(data){
      var html = '';

      _.forEach(JSON.parse(data), function(value) {
        html += '<tr>';
          html += '<td style="font-size:10px">From <strong>' + value.source + '</strong><br />';
          html += '<p style="color:gray">' + moment(value.created_at.date, 'YYYY-MM-DD HH:mm:ss.SSSS').fromNow() + '</p><a href="javascript:alert(\'View Functionlity Is Under Construction\')">View</a> | <a href="javascript:deleteLogs(' + value.id + ')">Delete</a></td>';
          // html += '<td style="font-size:12px"></td>';
        html += '</tr>';
      });

      if(html == '')
        html = 'No Logs';

        $('.logs-list').empty().append(html);
    }
  });
}

function deleteLogs(id){
  if(confirm("Are you sure you want to delete?")){
    ajaxPost('/logs/destroy/' + id, {_token:'{{ csrf_token() }}'});
    getLogsList();
  }
}
</script>
