@extends('layouts.app')

@section('content')
<div class="ui grid">
  <div class="four wide column">

    <h3 class="ui orange header">Info</h3>
    <div class="ui divider"></div>

    <div class="ui fluid card">
      <div class="image">
        <img src="/images/user.png">
      </div>
      <div class="content">
        <a class="header" id="user-name">{{ Auth::user()->name }}</a>
        <div class="meta">
          <span class="email" id="user-email">{{ Auth::user()->email }}</span>
        </div>
      </div>

      @if(Auth::user()->role == 1)
        <div class="extra content">
            <!-- <div class="ui red button">Delete</div> -->
        </div>
      @endif

    </div>


  </div>
  <div class="twelve wide column">
    @include('pops.bookmarks')
  </div>
</div>

<div class="ui grid">

  <div class="four wide column">
    @include('pops.contact_form')
  </div>

  <div class="twelve wide column">
    @include('pops.logs')
  </div>

</div>

@include('pops.delete')

<script>
  $(document).ready(function(){
    getBookmarkList();
    getLogsList();
  });
</script>

@endsection
