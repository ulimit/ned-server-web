@extends('layouts.app')

@section('content')

<!-- <table class="table table-striped table-hover table-condensed">
  <thead>
    <tr>
      <th>Date</th>
      <th>Latitude</th>
      <th>Longitude</th>
      <th></th>
    </tr>
  </thead>
  <tbody id="log-list"></tbody>
</table> -->

<div class="panel-group" id="log-list" role="tablist" aria-multiselectable="true"></div>

<hr />
<div class="alert alert-info" role="alert"> <strong>Simulation Only!</strong> This is used to simulate a logging on mobile device. <button class="btn btn-primary btn-xs pull-right" onclick="sendLog()">Send Log</button></div>
<script>
  getLogsList();

  function getLogsList(){
    $.ajax({
      url: '/log/list',
      data: {owner:localStorage.getItem('selected_user')},
      async: false,
      success: function(data){
        var html = '';

        _.forEach(JSON.parse(data), function(value) {

          html += '<div class="panel panel-default">';
            html += '<div class="panel-heading" role="tab" id="heading' + value.id + '">';
              html += '<h4 class="panel-title">';
                html += '<a role="button" data-toggle="collapse" data-parent="#log-list" href="#collpase' + value.id + '" aria-expanded="true" aria-controls="heading' + value.id + '">';
                  html += value.created_at;
                html += '</a>';
                html += '<div class="pull-right">';
                  html += '<a class="btn btn-danger btn-xs" href="javascript:deleteLog(' + value.id + ')">Delete</a>';
                html += '</div>';
              html += '</h4>';
            html += '</div>';

            html += '<div id="collpase' + value.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + value.id + '">';
              html += '<div class="panel-body">';

              //----------------------------------------------------------
              _.forEach(JSON.parse(value.journey), function(value2) {
                var d = new Date(+value2.id);
                html += '<div><strong>Latitude: </strong>' + value2.lat + '</div>';
                html += '<div><strong>Longitude: </strong>' + value2.lng + '</div>';
                //html += '<div><button type="button" class="btn btn-primary btn-xs" onclick="viewMap(' + latlngOrigin[0] + ',' + latlngOrigin[1] + ',\'' + value.title + '\')">';
                  //html += 'View Map';
                //html += '</button>';
                html += '<hr />';
              });
              //----------------------------------------------------------

            html += '</div>';
            html += '</div>';
          html += '</div>';
        html += '</div>';

        });

        if(html == '')
          html = 'No Logs';

          $('#log-list').empty().append(html);
      }
    });
  }

  function sendLog(){
    var result = ajaxPost('/log/post',{
      owner: 1,
      journey:[{"lat":9.6498256,"lng":123.8567503,"dt":1455785909800},{"lat":9.6498256,"lng":123.8567503,"dt":1455785910342},{"lat":9.6498256,"lng":123.8567503,"dt":1455785915304},{"lat":9.649834799999999,"lng":123.8567659,"dt":1455785924693},{"lat":9.649834799999999,"lng":123.8567659,"dt":1455785925404},{"lat":9.649834799999999,"lng":123.8567659,"dt":1455785930309},{"lat":9.6498252,"lng":123.8567479,"dt":1455785939743}]
    });

    getLogsList();
  }

  function deleteLog(id){
    if(confirm("Are you sure you want to delete?")){

      showLoading();

      $.when( ajaxPost('/log/destroy/' + id) ).then(function( data, textStatus, jqXHR ) {

        endLoading();

      });

      getLogsList();
    }
  }
</script>
@endsection
