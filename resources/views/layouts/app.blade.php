<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NED</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- JavaScripts -->
    <script type="text/javascript" src="jquery-2.1.4.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="lodash.core.min.js"></script>
    <script type="text/javascript" src="moment.js"></script>
    <script type="text/javascript" src="addon-scripts.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
    <script type="text/javascript" src="map.js"></script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <!-- Styles -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="addon-styles.css" />

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <strong>NED SERVER</strong>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


    <div class="container">

      @if (!Auth::guest())
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">

              <div class="media">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object" src="images/elliot.jpg" style="width:.5in" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading title">{{ Auth::user()->name }}</h4>
                  {{ Auth::user()->email }}
                </div>
              </div>

              <hr />

              <div class="list-group">
                <a href="{{ url('/bookmarks') }}" class="list-group-item">My Bookmarks</a>
                <a href="{{ url('/logs') }}" class="list-group-item">Travel Logs</a>
                <a href="{{ url('/settings') }}" class="list-group-item">My Settings</a>
                <a href="{{ url('/logout') }}" class="list-group-item" style="color:red">Logout</a>
              </div>

            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="panel panel-default">
            <div class="panel-body">
              <h4 class="title">{{ $title or 'NED Server'}}</h4>
              @yield('content')
              </div>
            </div>
          </div>
        </div>


      <!-- Modal -->
      <div class="modal fade" id="bookmark-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Edit Bookmark Title</h4>
            </div>
            <div class="modal-body">
              <input type="title" id="title" class="form-control" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary bookmark-approve">Update Bookmark</button>
            </div>
          </div>
        </div>
      </div>

        @else
          @yield('content')
        @endif


    </div>

    @include('layouts.loading')

    <script>

      @if(!Auth::guest())
        localStorage.setItem("logged", "{{ Auth::user()->id }}");
        localStorage.setItem("selected_user", "{{ Auth::user()->id }}");

        if(localStorage.getItem('selected_user') == null || localStorage.getItem('selected_user') == 'undefined' || localStorage.getItem('selected_user') == '')
          localStorage.setItem("selected_user", "{{ Auth::user()->id }}");
      @endif
    </script>
</body>
</html>
