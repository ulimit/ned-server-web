<div class="modal fade" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="map-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="map-modal-label">Map View</h4>
      </div>
      <div class="modal-body" id="map-body" style="height:400px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function viewMap(origin,destination,title){
    $.when( $("#map-modal").modal('show') ).then(function( data, textStatus, jqXHR ){
      $.when( initMap() ).then(function( data, textStatus, jqXHR ){
        $("#map-modal-label").text('Map View: ' + title);
        drawRoute(origin,destination);
      });
    });
  }
</script>
