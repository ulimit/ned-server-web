
<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">NED Server</h4>
      </div>
      <div class="modal-body">
        <strong><h3>Please Wait...</h3></strong>
      </div>
    </div>
  </div>
</div>

<script>
  function showLoading(){
    $("#loadingModal").modal('show');
  }

  function endLoading(){
    $("#loadingModal").modal('hide');
  }
</script>
