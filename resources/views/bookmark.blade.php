@extends('layouts.app')

@section('content')

<div class="panel-group" id="bookmark-list" role="tablist" aria-multiselectable="true"></div>

<hr />
<div class="alert alert-info" role="alert"> <strong>Simulation Only!</strong> This is used to simulate a bookmark function on mobile device. <button class="btn btn-primary btn-xs pull-right" onclick="sendBookmark()">Send Bookmark</button></div>

@include('layouts.map')

<script>

getBookmarkList();

function getBookmarkList(){
  $.ajax({
    url: '/bookmark/list',
    data: {owner:localStorage.getItem('selected_user')},
    async: false,
    success: function(data){
      var html = '';

      _.forEach(JSON.parse(data), function(value) {

        var latlngOrigin = value.origin_latlng.split(",");
        var latlngDestination = value.destination_latlng.split(",");

        html += '<div class="panel panel-default">';
          html += '<div class="panel-heading" role="tab" id="heading' + value.id + '">';
            html += '<h4 class="panel-title">';
              html += '<a role="button" data-toggle="collapse" data-parent="#bookmark-list" href="#collpase' + value.id + '" aria-expanded="true" aria-controls="heading' + value.id + '">';
                html += value.title;
              html += '</a>';
              html += '<div class="pull-right">';
                html += '<a class="btn btn-primary btn-xs" href="javascript:editBookmark(' + value.id + ',\'' + value.title + '\')" style="margin-right:5px">Edit</a>';
                html += '<a class="btn btn-danger btn-xs" href="javascript:deleteBookmark(' + value.id + ')">Delete</a>';
              html += '</div>';
            html += '</h4>';
          html += '</div>';

          html += '<div id="collpase' + value.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + value.id + '">';
            html += '<div class="panel-body">';
            html += '<div><strong>Origin: </strong>' + value.formatted_origin + '</div>';
            html += '<div><strong>Destination: </strong>' + value.formatted_destination + '</div>';
            html += '<div><button type="button" class="btn btn-primary btn-xs" onclick="viewMap(\'' + value.origin + '\',\'' + value.destination + '\',\'' + value.title + '\')">';
              html += 'View Map';
            html += '</button></div>';
            html += '</div>';
          html += '</div>';
        html += '</div>';

      });

      if(html == '')
        html = 'No Bookmarks';

        $('#bookmark-list').empty().append(html);
    }
  });
}

function sendBookmark(){

  showLoading();

  $.when( ajaxPost('/bookmark/post',
    {
      "owner":1,
      "title":"test 4",
      "origin":"{\"lat\":10.3156992,\"lng\":123.88543660000005}",
      "destination":"{\"placeId\":\"ChIJu5dH8GyZqTMRSC9ixmg-G7k\"}"
    }
  ) ).then(function( data, textStatus, jqXHR ) {

    endLoading();

  });

  getBookmarkList();
}

function editBookmark(id,title){
  $("#title").val(title);
  $(".bookmark-approve").attr('onclick','updateBookmark(' + id + ')');
  $("#bookmark-modal").modal('show');
}

function updateBookmark(id){
  ajaxPost('/bookmark/update',{title: $("#title").val(),id:id});
  $("#bookmark-modal").modal('hide');
  getBookmarkList();
}

function deleteBookmark(id){
  if(confirm("Are you sure you want to delete?")){

    showLoading();

    $.when( ajaxPost('/bookmark/destroy/' + id) ).then(function( data, textStatus, jqXHR ) {

      endLoading();

    });

    getBookmarkList();
  }
}

</script>
@endsection
