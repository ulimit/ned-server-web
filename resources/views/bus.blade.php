@extends('layouts.app')

@section('content')
<div class="panel-group" id="bus-list" role="tablist" aria-multiselectable="true"></div>

<hr />
<div class="alert alert-info" role="alert"> <strong>Simulation Only!</strong> This is used to simulate a bus registration. <button class="btn btn-primary btn-xs pull-right" onclick="sendBus()">Bus</button></div>
<script>
  getBusList();

  function getBusList(){
    $.ajax({
      url: '/bus/list',
      data: {},
      async: false,
      success: function(data){
        var html = '';

        _.forEach(JSON.parse(data), function(value) {

          html += '<div class="panel panel-default">';
            html += '<div class="panel-heading" role="tab" id="heading' + value.id + '">';
              html += '<h4 class="panel-title">';
                html += '<a role="button" data-toggle="collapse" data-parent="#bus-list" href="#collpase' + value.id + '" aria-expanded="true" aria-controls="heading' + value.id + '">';
                  html += value.bus_stop_id;
                html += '</a>';
                html += '<div class="pull-right">';
                  //html += '<a class="btn btn-success btn-xs" style="margin-right:5px" href="javascript:viewMapPolygon(' + value.id + ')">View Map</a>';
                  html += '<a class="btn btn-danger btn-xs" href="javascript:deleteBus(' + value.id + ')">Delete</a>';
                html += '</div>';
              html += '</h4>';
            html += '</div>';

            html += '<div id="collpase' + value.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + value.id + '">';
              html += '<div class="panel-body">';

              //----------------------------------------------------------
              //_.forEach(JSON.parse(value.journey), function(value2) {

                //var d = new Date(+value2.dt);
                //html += '<div><strong>Latitude: </strong>' + value2.lat + '</div>';
                //html += '<div><strong>Longitude: </strong>' + value2.lng + '</div>';
                //html += '<div><button type="button" class="btn btn-primary btn-xs" onclick="viewMap(' + latlngOrigin[0] + ',' + latlngOrigin[1] + ',\'' + value.title + '\')">';
                  //html += 'View Map';
                //html += '</button>';
                //html += '<hr />';

              //});
              //----------------------------------------------------------

            html += '</div>';
            html += '</div>';
          html += '</div>';
        html += '</div>';

        });

        if(html == '')
          html = 'No Bus Stops';

          $('#bus-list').empty().append(html);
      }
    });
  }

  function sendBus(){
    alert();
    var result = ajaxGet('/bus/list',{});
    console.log(result);
  }

  function deleteBus(id){
    if(confirm("Are you sure you want to delete?")){

      showLoading();

      $.when( ajaxPost('/bus/destroy/' + id) ).then(function( data, textStatus, jqXHR ) {

        endLoading();

      });

      getBusList();
    }
  }
</script>
@endsection
