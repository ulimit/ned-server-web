<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubwaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subways', function (Blueprint $table) {
            $table->increments('id');
	    $table->string('code',10);
            $table->string('name',50);
            $table->string('name_chinese',50)->nullable();
            $table->string('line_code',10);
            $table->integer('sequence',false,true);
            $table->string('line_name',50);
            $table->string('line_name_chinese',50)->nullable();
            $table->string('latitude',20)->nullable();
            $table->string('longitude',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subways');
    }
}
