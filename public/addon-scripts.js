function ajaxPost(url, data){
  var result = '';

  $.ajax({
    url: url,
    data: data,
    async: false,
    method: 'POST',
    success: function(data){
      result = data;
    }
  });

  return result;
}

function ajaxGet(url, data){
  var result = '';

  $.ajax({
    url: url,
    data: data,
    async: false,
    success: function(data){
      result = JSON.parse(data);
    }
  });

  return result;
}
