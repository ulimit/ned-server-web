var map;
var origin;

var geocoder = new google.maps.Geocoder;
var travelMode = google.maps.TravelMode.TRANSIT;
var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer;

function initMap(){
  navigator.geolocation.getCurrentPosition(onMapSuccess, onMapError, {timeout: 30000, maximumAge: 0, enableHighAccuracy: true});
}

function onMapSuccess(position){
  origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

  map = new google.maps.Map(document.getElementById('map-body'), {
    zoomControl: false,
    mapTypeControl: false,
    streetViewControl: false,
    center: origin,
    zoom: 15
  });

  directionsDisplay.setMap(map);
}

function onMapError(position){

}

function drawRoute(origin_id, destination_id){
  if(!destination_id){
    alert('Map cannot be initialized');
    return;
  }

  directionsService.route({
    origin: {'placeId': origin_id},
    destination: {'placeId': destination_id},
    provideRouteAlternatives: true,
    travelMode: travelMode
  }, function(response, status) {
    console.log(response);
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
